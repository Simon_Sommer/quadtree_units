﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This Event System offerst a conviniend way to have to classes comunicate wihtout knowing each other

///Example events

/// <summary>
/// ExampleEvent
/// </summary>
public sealed class OnExampleEvent : Event<OnExampleEvent> { }

/// <summary>
/// ExampleEvent that sends an Int
/// </summary>
public sealed class OnExampleIntEvent : Event<int, OnExampleIntEvent> { }

/// <summary>
/// ExampleEvent that sends an GameObject, that e.g can be used to identify the scoureObject that is sendin the event
/// </summary>
public sealed class OnExampleGameObjectEvent : Event<GameObject, OnExampleIntEvent> { }


/// <summary>
/// ExampleEvent with data that is send
/// </summary>
public sealed class OnExamplewithDataEvent : Event<ExampleData, OnExamplewithDataEvent> { }
public class ExampleData
{
	public int aInt;
	public float aFloat;
}
///End of Exaple Events

/// <summary>
/// Event that when the player wants to spawn a unit at a certain position
/// </summary>
public sealed class SpawnUnitInputEvent : Event<Vector3, SpawnUnitInputEvent> { }

public sealed class OnUnitCreatedEvent : Event<Unit, OnUnitCreatedEvent> { }

public abstract class Event<TData, TClass>
{
	public delegate void EventHandler<T>(TData data);

	private static EventHandler<TClass> _actions = null;

	public static void Add(EventHandler<TClass> action)
	{
		_actions += action;
	}

	public static void Remove(EventHandler<TClass> action)
	{
		_actions -= action;
	}

	public static void Execute(TData data)
	{
		if (_actions != null)
			// execute actions of non observing senders
			_actions(data);
	}
}
public abstract class Event<TClass>
{
	public delegate void EventHandler<T>();
	private static EventHandler<TClass> _actions = null;

	public static void Add(EventHandler<TClass> action)
	{
		_actions += action;
	}
	public static void Remove(EventHandler<TClass> action)
	{
		_actions -= action;
	}
	public static void Execute()
	{
		if (_actions != null)
			// execute actions of non observing senders
			_actions();
	}

}

/// <summary>
/// Event to execute action with sender and data.
/// </summary>
public abstract class Event<TData, TSender, TClass>
{

	public delegate void EventHandler<T>(TData data, TSender sender);

	#region Fields

	// all senders with actions
	private static Dictionary<TSender, EventHandler<TClass>> _senders = new Dictionary<TSender, EventHandler<TClass>>();

	private static EventHandler<TClass> _actions = null;

	#endregion Fields

	#region Methods

	/// <summary>
	/// Add the specified action.
	/// </summary>
	/// <param name="action">Action to add.</param>
	public static void Add(EventHandler<TClass> action)
	{
		_actions += action;
	}

	/// <summary>
	/// Add the specified action to observe sendeer.
	/// </summary>
	/// <param name="action">Action to add.</param>
	/// <param name="sender">Sender to observe.</param>
	public static void Add(EventHandler<TClass> action, TSender sender)
	{
#if DEBUG
		if (sender == null)
		{
			Debug.LogError("Observing sender of an event can't be null");
			return;
		}
#endif

		if (_senders.ContainsKey(sender))
			// add action to specified sender
			_senders[sender] += action;
		else
			// add new sender with action
			_senders.Add(sender, action);
	}

	/// <summary>
	/// Remove the specified action.
	/// </summary>
	/// <param name="action">Action to remove.</param>
	public static void Remove(EventHandler<TClass> action)
	{
		_actions -= action;
	}

	/// <summary>
	/// Remove the specified action from observing sender.
	/// </summary>
	/// <param name="action">Action to remove.</param>
	/// <param name="sender">Observing sender.</param>
	public static void Remove(EventHandler<TClass> action, TSender sender)
	{
#if DEBUG
		if (sender == null)
		{
			Debug.LogError("Observing sender of an event can't be null");
			return;
		}
#endif

		EventHandler<TClass> handler;

		if (_senders.TryGetValue(sender, out handler))
			// action of sender dosen't equal specified action
			if (handler != action)
				// remove action from sender
				_senders[sender] -= action;
			else
				// remove sender
				_senders.Remove(sender);
	}

	/// <summary>
	/// Execute the event with specified data and sender.
	/// </summary>
	/// <param name="data">Data of event.</param>
	/// <param name="sender">Sender of event.</param>
	public static void Execute(TData data, TSender sender)
	{
#if DEBUG
		if (sender == null)
		{
			Debug.LogError("Sender of an event can't be null");
			return;
		}
#endif

		EventHandler<TClass> actions;

		if (_senders.TryGetValue(sender, out actions))
			// execute action of observing sender
			actions(data, sender);

		if (_actions != null)
			// execute actions of non observing senders
			_actions(data, sender);
	}

	#endregion Methods
}
