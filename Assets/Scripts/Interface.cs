using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Interface : MonoBehaviour
{
    [SerializeField] LayerMask groundLayer;
    [SerializeField] float rayDistance = 1000.0f;

    [SerializeField] int xCount = 5;
    [SerializeField] int yCount = 2;

    [SerializeField] TextMeshProUGUI unitCouttext;
    private int unitCount = 0;
    // Start is called before the first frame update
    void Start()
    {
        OnUnitCreatedEvent.Add(OnUnitCreated);
    }
    private void OnDestroy()
    {
        OnUnitCreatedEvent.Remove(OnUnitCreated);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, rayDistance, groundLayer))
            {
                SpawnUnitInputEvent.Execute(hit.point);
            }
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, rayDistance, groundLayer))
            {
				for (int i = 0; i < xCount; i++)
				{
				    for (int j = 0; j < yCount; j++)
				    {
                        SpawnUnitInputEvent.Execute(hit.point + new Vector3(i, 0, j));
				    }
				}
            }
        }
    }
    void OnUnitCreated(Unit unit)
    {
        unitCount++;
        unitCouttext.text = unitCount.ToString();
    }
}
