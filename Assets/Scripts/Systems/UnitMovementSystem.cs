using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This System contains logic that makes units move towards thier target points
/// </summary>
public class UnitMovementSystem : MonoBehaviour, IInitializable
{
    List<Unit> units = new List<Unit>();
    [SerializeField] LayerMask groundLayer;
    [SerializeField] float rayDistance = 1000.0f;
    private Rect mapBounds;
    public void Init(SimulationController simulationController)
    {
        mapBounds = simulationController.MapBounds;
        OnUnitCreatedEvent.Add(OnUnitCreated);
    }
    private void OnDestroy()
    {
        OnUnitCreatedEvent.Remove(OnUnitCreated);
    }

    private void FixedUpdate()
    {
        Vector3 tv3 = Vector3.zero;

        float dt = Time.deltaTime;
        foreach (var unit in units)
        {
            unit.moveVector = unit.targetPoint - unit.position;
            unit.moveVector.Normalize();
            unit.moveVector *= dt * unit.moveSpeed;

            if (Vector2.SqrMagnitude(unit.targetPoint - unit.position) < Vector2.SqrMagnitude(unit.moveVector))
            {
                unit.moveVector = unit.targetPoint - unit.position;
            }

            if (mapBounds.Contains(unit.position + unit.moveVector))
            {
                unit.position += unit.moveVector;
            }

            tv3.x = unit.position.x;
            tv3.y = 0;
            tv3.z = unit.position.y;

            unit.transform.position = tv3;
        }
    }
    void OnUnitCreated(Unit unit)
    {
        units.Add(unit);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, rayDistance, groundLayer))
            {
                SetTarget(hit.point);
            }
        }
    }
    void SetTarget(Vector3 targetPoint)
    {
        Vector2 tv2 = Vector2.zero;
        tv2.x = targetPoint.x;
        tv2.y = targetPoint.z;
        foreach (var item in units)
        {
            item.targetPoint = tv2;
        }
    }
}
