using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// contains logic that adds units to a quadtree depending on the unit position, also updates units position inside the strucure of the tree, 
/// </summary>
public class QuadTreeSystem : MonoBehaviour, IInitializable
{
    static QuadTree rootNode;
    [SerializeField] List<Unit> units = new List<Unit>();
    public void Init(SimulationController simulationController)
    {
        rootNode = new QuadTree(new Vector2(0, 0), null, simulationController.MapSize);
        OnUnitCreatedEvent.Add(OnUnitCreated);
    }

    void OnDestroy()
    {
        OnUnitCreatedEvent.Remove(OnUnitCreated);
    }
    void OnUnitCreated(Unit unit)
    {
        rootNode.AddUnit(unit);
        units.Add(unit);
    }
    void Update()
    {
        DrawNode(rootNode);
    }

    void FixedUpdate()
    {
        foreach (var unit in units)
        {
            if (unit.quadTreeNode != null)
            {
                if (unit.quadTreeNode.Rect.Contains(unit.position) == false)
                {
                    unit.quadTreeNode.RemoveUnit(unit);
                    rootNode.AddUnit(unit);
                }

            }
        }
    }
    /// <summary>
    /// returns all units that are inside a specific rect
    /// </summary>
    /// <param name="targetPoint"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    public static List<Unit> GetUnitsInRect(Vector2 targetPoint, float range)
    {

        List<Unit> units = new List<Unit>();


        Vector2 position = new Vector2(targetPoint.x - range * 0.5f, targetPoint.y - range * 0.5f);
        Vector2 size = new Vector2(range, range);
        Rect rect = new Rect(position, size);

        CollectOverLapingLeavesUnits(rootNode, rect, units);
        return units;
    }
    /// <summary>
    /// recusive, finds all the leaves in a quadtree that overlap with a specific rect
    /// </summary>
    /// <param name="tree"></param>
    /// <param name="rect"></param>
    /// <param name="units"></param>
    static void CollectOverLapingLeavesUnits(QuadTree tree, Rect rect, List<Unit> units)
    {
        if (tree.Rect.Overlaps(rect))
        {
            if (tree.HasChilds == false)
            {
                units.AddRange(tree.Units);
            }
            else
            {
                CollectOverLapingLeavesUnits(tree.TopRight, rect, units);
                CollectOverLapingLeavesUnits(tree.BottomLeft, rect, units);
                CollectOverLapingLeavesUnits(tree.BottomRight, rect, units);
                CollectOverLapingLeavesUnits(tree.TopLeft, rect, units);
            }
        }
    }
    /// <summary>
    /// a method to visualize the quad tree
    /// </summary>
    /// <param name="quadTree"></param>
    void DrawNode(QuadTree quadTree)
    {
        Vector2 center = quadTree.Rect.center;
        Vector2 halfSize = quadTree.Rect.size * 0.5f;
        Vector3 topRight = new Vector3(center.x + halfSize.x, 0, center.y + halfSize.y);
        Vector3 bottomRight = new Vector3(center.x + halfSize.x, 0, center.y - halfSize.y);
        Vector3 bottomLeft = new Vector3(center.x - halfSize.x, 0, center.y - halfSize.y);
        Vector3 topLeft = new Vector3(center.x - halfSize.x, 0, center.y + halfSize.y);

        Debug.DrawLine(topRight, bottomRight);
        Debug.DrawLine(bottomRight, bottomLeft);
        Debug.DrawLine(bottomLeft, topLeft);
        Debug.DrawLine(topLeft, topRight);

        if (quadTree.TopRight != null)
            DrawNode(quadTree.TopRight);
        if (quadTree.BottomRight != null)
            DrawNode(quadTree.BottomRight);
        if (quadTree.BottomLeft != null)
            DrawNode(quadTree.BottomLeft);
        if (quadTree.TopLeft != null)
            DrawNode(quadTree.TopLeft);
    }
}
