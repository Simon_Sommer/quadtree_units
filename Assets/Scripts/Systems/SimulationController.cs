using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Initialises all Systems that are relevant for the simulation
/// </summary>
public class SimulationController : MonoBehaviour
{
    [SerializeField] Vector2 mapSize = new Vector2(70, 50);
    [SerializeField] GameObject ground;

    public Rect MapBounds { get; private set; }
    public Vector2 MapSize { get => mapSize; set => mapSize = value; }
    public GameObject Ground { get => ground; set => ground = value; }

    // Start is called before the first frame update
    void Start()
    {
        MapBounds = new Rect(-mapSize * 0.5f, mapSize);
        ground.transform.localScale = new Vector3(mapSize.x, 1, mapSize.y);

        GetComponentInChildren<UnitMovementSystem>().Init(this);
        GetComponentInChildren<UnitSpawnSystem>().Init(this);
        GetComponentInChildren<QuadTreeSystem>().Init(this);
        GetComponentInChildren<TargetPointAvoidanceSystem>().Init(this);
    }
}
