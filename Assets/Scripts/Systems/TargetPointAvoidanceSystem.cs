using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// contains logic, that makes units have thier targepoints seperated from each other
/// it "relaxes" the targepoints away from each other
/// </summary>
public class TargetPointAvoidanceSystem : MonoBehaviour, IInitializable
{
    List<Unit> units = new List<Unit>();
    [SerializeField] float avoidanceValue = 0.1f;
    [SerializeField] float avoidanceDistance = 1.0f;
    [SerializeField] float avoidanceScaneDistance = 2.0f;

    Vector2 safe = new Vector2(float.Epsilon, float.Epsilon);
    Rect mapBounds;
    public void Init(SimulationController simulationController)
    {
        mapBounds = simulationController.MapBounds;
        OnUnitCreatedEvent.Add(OnUnitCreated);
    }
    private void OnDestroy()
    {
        OnUnitCreatedEvent.Remove(OnUnitCreated);
    }

    public void FixedUpdate()
    {
        foreach (var unit1 in units)
        {
            List<Unit> unitsRect = QuadTreeSystem.GetUnitsInRect(unit1.position, avoidanceScaneDistance);
            foreach (var unit2 in unitsRect)
            {
                if (unit1 != unit2)
                {
                    if (Vector2.Distance(unit1.targetPoint, unit2.targetPoint) < avoidanceDistance)
                    {
                        Vector2 dir1 = unit1.position - unit2.position;
                        //fix for units with the same position
                        if (dir1.Equals(Vector2.zero))
                        {
                            unit1.position += safe;
                            unit2.position -= safe;
                            dir1 = unit1.position - unit2.position;
                        }

                        dir1.Normalize();
                        dir1 *= avoidanceValue;

                        if (mapBounds.Contains(unit1.targetPoint + dir1))
                            unit1.targetPoint += dir1;
                        if (mapBounds.Contains(unit2.targetPoint - dir1))
                            unit2.targetPoint -= dir1;
                    }
                }
            }
        }
    }

    void Update()
    {
        VisualizeTargetPoints();
    }
    /// <summary>
    /// draws lines for the units targetpoints
    /// </summary>
    void VisualizeTargetPoints()
	{
        Vector3 posi_1 = Vector3.zero;
        Vector3 posi_2 = Vector3.zero;

        foreach (var unit in units)
        {
            posi_1.x = unit.position.x;
            posi_1.z = unit.position.y;
            posi_2.x = unit.targetPoint.x;
            posi_2.z = unit.targetPoint.y;

            Debug.DrawLine(posi_1, posi_2, Color.red);
        }
	}

    void OnUnitCreated(Unit unit)
    {
        units.Add(unit);
    }
}
