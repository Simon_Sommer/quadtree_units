using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// contains logic for spawning units
/// </summary>
public class UnitSpawnSystem : MonoBehaviour , IInitializable
{
    [SerializeField] GameObject unitPrefab;
    [SerializeField] LayerMask groundLayer;
    [SerializeField] float rayDistance = 1000.0f;
  
    public void Init(SimulationController simulationController)
    {
        SpawnUnitInputEvent.Add(SpawnUnit);
    }
    void OnDestroy()
    {
        SpawnUnitInputEvent.Remove(SpawnUnit);
    }
    
    void SpawnUnit(Vector3 position)
    {
        GameObject go = Instantiate(unitPrefab, position, Quaternion.identity);
        Unit unit = go.GetComponent<Unit>();

        Vector2 newPosi = new Vector2(position.x, position.z);
        unit.position = newPosi;
        unit.targetPoint = newPosi;
        OnUnitCreatedEvent.Execute(unit);
    }
}
