using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// tree structure that is used to partition the map space. The more units are in a certrain area, the more and smaller QuadTree nodes shall be in that area
/// its main purpose is to reduce the number of units that need to be checked for things like collison or target point avoidance
/// </summary>
public class QuadTree
{
    const int nodeCapacity = 4;
    const int maxdepth = 10;
    public List<Unit> Units { get; private set; }
    //child nodes
    public QuadTree TopRight { get; private set; }
    public QuadTree BottomRight { get; private set; }
    public QuadTree BottomLeft { get; private set; }
    public QuadTree TopLeft { get; private set; }
    public QuadTree Parent { get; private set; }

    //bounds
    public Rect Rect { get; private set; }
    public int Depth { get; private set; }
    public bool HasChilds { get; private set; }
    public int UnitCount { get; private set; }
    public QuadTree(Vector2 position, QuadTree _parent, Vector2 size)
    {
        TopLeft = null;
        BottomLeft = null;
        BottomRight = null;
        TopRight = null;

        Units = new List<Unit>();

        Rect = new Rect(position - size * 0.5f, size);

        if (_parent == null)
            Depth = 0;
        else
        {
            Depth = _parent.Depth + 1;
            this.Parent = _parent;
        }
        HasChilds = false;
        UnitCount = 0;
    }
    /// <summary>
    /// adds the 4 childs  to the node and moves the units down to the right childs
    /// </summary>
    public void Split()
    {
        Vector2 newSize = Rect.size * 0.5f;
        Vector2 newHalfsize = newSize * 0.5f;
        Vector2 topRightPositon =  Rect.center + new Vector2(newHalfsize.x, newHalfsize.y);
        HasChilds = true;
        TopRight = new QuadTree(topRightPositon, this, newSize);

        Vector2 bottomRightPositon = Rect.center + new Vector2(newHalfsize.x, -newHalfsize.y);
        BottomRight = new QuadTree(bottomRightPositon, this, newSize);

        Vector2 bottomLeftPositon = Rect.center + new Vector2(-newHalfsize.x, -newHalfsize.y);
        BottomLeft = new QuadTree(bottomLeftPositon, this, newSize);

        Vector2 topLeftPositon = Rect.center + new Vector2(-newHalfsize.x, newHalfsize.y);
        TopLeft = new QuadTree(topLeftPositon, this, newSize);
        foreach (var unit in Units)
        {
            AddUnitToChild(unit);
        }
        Units.Clear();
    }
    /// <summary>
    /// removes the childs of this node and adds the childs units to this node, rekusivly repeats this with the parent
    /// </summary>
    void ColapseChilds()
    {
        Units.AddRange(TopRight.Units);
        Units.AddRange(BottomRight.Units);
        Units.AddRange(BottomLeft.Units);
        Units.AddRange(TopLeft.Units);

        foreach (var item in Units)
        {
            item.quadTreeNode = this;
        }

        TopRight = null;
        BottomRight = null;
        BottomLeft = null;
        TopLeft = null;

        HasChilds = false;
        if (Parent != null)
        {
            if (Parent.UnitCount < nodeCapacity)
                Parent.ColapseChilds();
        }
    }
    /// <summary>
    /// rekusive, removes a unit from the tree and changes the structure afterwards if needed
    /// </summary>
    /// <param name="unit"></param>
    public void RemoveUnit(Unit unit)
    {
        unit.quadTreeNode = null;
        RemoveUnitFromList(unit);
        if (Parent != null)
        {
            if (Parent.UnitCount < nodeCapacity)
                Parent.ColapseChilds();
        }
    }
    /// <summary>
    /// a rekusive help funktion that removes a unit form the unit list of a node and reduces the unitcount of the node
    /// </summary>
    /// <param name="unit"></param>
    public void RemoveUnitFromList(Unit unit)
    {
        if (Units.Count > 0)
            Units.Remove(unit);
        UnitCount--;

        if (Parent != null)
            Parent.RemoveUnitFromList(unit);
    }
    /// <summary>
    /// adds a unit to the tree and recusivly changes the structure if needed
    /// </summary>
    /// <param name="unit"></param>
    public void AddUnit(Unit unit)
    {
        UnitCount++;
        if (HasChilds)
            AddUnitToChild(unit);
        else
        {
            Units.Add(unit);
            unit.quadTreeNode = this;
            if (Depth >= maxdepth)
                return;

            if (Units.Count > nodeCapacity)
                Split();
        }
    }
    /// <summary>
    /// adds a unit to the right child node depending on the position of the unit, reksive
    /// </summary>
    /// <param name="unit"></param>
    void AddUnitToChild(Unit unit)
    {
        if (unit.position.y >= Rect.center.y)
        {
            //top
            if (unit.position.x >= Rect.center.x)
                TopRight.AddUnit(unit);
            else
                TopLeft.AddUnit(unit);
        }
        else
        {
            //botton
            if (unit.position.x >= Rect.center.x)
                BottomRight.AddUnit(unit);
            else
                BottomLeft.AddUnit(unit);
        }
    }
}
