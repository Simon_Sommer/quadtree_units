using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// a basic unit class for an rts like game
/// </summary>
public class Unit : MonoBehaviour
{
    public float moveSpeed = 10;
    
    public Vector2 position;
    public Vector2 targetPoint;
    public Vector2 moveVector;

    public QuadTree quadTreeNode;
}
