
public interface IInitializable
{
    public void Init(SimulationController simulationController);
}
